\makeatletter
\def\input@path{ {./} {cls/} {sty/} {tikzlibraries/} {fig/} {tex/} }
\makeatother
\documentclass[czech,italian,UKenglish]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{babel}

\usepackage{amsmath,amsthm,amssymb,mathtools}
\usepackage[inline]{enumitem}
\usepackage[mode=image|tex]{standalone}

% local packages
\usepackage{mybeamer}
\usepackage{tikzemph}
\usepackage{leftright}
\usepackage{macros}
\usepackage{globalstyles}
\usepackage{kanji}

\setcounter{tocdepth}{4}

\title{Two-way Automata and One-tape Machines}
\subtitle{Read-only Versus Linear-time}
\author{%
	\underline{Bruno Guillon}
	\and
	Giovanni Pighizzini
	\and
	Luca Prigioniero
	\and
	Daniel Pr\r u\v sa%
}
\date{%
	{%
		\small\textsc{dlt}%
	}
	\newline%
	{%
		\scriptsize
		September 14, 2018%
	}%
}
\institute{%
	{\selectlanguage{italian}{Dipartimento di Informatica, Università degli Studi di Milano}},%
	\newline
	{\selectlanguage{czech}{Fakulta elektrotechnická, České vysoké učení technické v Praze}}%
}

\begin{document}

\addtocounter{framenumber}{-1}
\maketitle
\section{Introduction}
\begin{frame}{The Sakoda \& Sipser problem}
	\widebox[1]{%
		\centering%
		\foreach \time [count=\page] in {1,2,3,4,5,6,7,8-9,10,11,12,13}{%
			\only<\time>{\includegraphics[page=\page]{descr-compl-reg}}%
		}%
	}%
	\vspace{-12ex}

	\uncover<9->{
		\rule{\textwidth}{.5pt}\newline
		\textbf{Attempts to solve the problem:}%
	}
	\begin{itemize}[nosep,leftmargin=7.5mm]%
		\item<9-> unary case
			{\small
				\hfill\cite{Geffert,Mereghetti,Pighizzini'03}%
			}
		\item<10-> restriction on the simulating machine
			{\small
				\hfill\textit{sweeping}\cite{Sipser'80}%

				\hfill%
				\textit{oblivious}\cite{Hromkovic\&Schnitger'03}\!\!,
				\textit{few-reversals}\cite{Kapoutsis'14}%
			}
		\item<11-> restriction on the simulated machine
			{\small

				\hfill\textit{outer-nondeterministic}\cite{G.,Geffert,Pighizzini'14}%
			}
	\end{itemize}%
	\uncover<12->{\Large\thiswork \alert<12-13>{\bfseries extension} of the \alert<12-13>{\bfseries simulating} machine}%
\end{frame}
\begin{frame}{Linear-time one-tape Turing machines}
	\begin{inlinedefinition}[\lintime\Tm]\newline
		\textbf{model:} one-tape (read/write) Turing machines\newline
		\textbf{resource:} linear time
	\end{inlinedefinition}

	\begin{overlayarea}{\textwidth}{.6\textheight}
		\rightwidebox{%
			\begin{inlineexample}<11->
				$%
				L_k\!=\!%
				\set{u_0\!\cdots\!u_\ell}[%
					\ell\!\in\!\mathbb N%
					\,\,\wedge\,\,%
					\forall i,u_i\!\in\!\ialphab^k%
					\,\,\wedge\,\,%
					\exists j\!>\!0, u_0\!=\!u_j%
				]%
				$
			\end{inlineexample}
		}
		\vspace{-2ex}
		\widebox[1]{%
			\foreach \time [count=\page] in {1,...,9,10}{%
				\only<\time>{\includegraphics[page=\page,width=\paperwidth]{lt-Tm-def}}%
			}%
			\only<11>{\includegraphics[page=1,width=\paperwidth]{lt-Tm-ex}}%
			\foreach \page [count=\time from 12] in {1,...,10,13,14,15,18,19,23,24}{%
				\only<\time>{\includegraphics[page=\page,width=\paperwidth]{lt-Tm-ex}}%
			}%
		}%
	\end{overlayarea}
	\medbreak
	\begin{overlayarea}{\textwidth}{3ex}%
		\uncover<16->{\kT*$\equiv$True\qquad\kF*$\equiv$False\hfill\vbox{}}%
	\end{overlayarea}
\end{frame}
\begin{frame}{Hennie's result}
	\vspace{-4ex}
	\begin{inlinetheorem}
		\cite[font=\large]{Hennie'65}
		\Large\hfill%
		\lintime \Tm = \reg%
		\hfill\hfill\vbox{}%
		%
		%%Maybe, cite references for nondeterministic cases, and other extensions.
		%\hfill\cite[font=\large]{}
	\end{inlinetheorem}
	\begin{inlineprooof}<2->
		\begin{itemize}
			\item<2-> $\lintime\implies\linspace~~\uncover<3->{\xrightarrow{\text{~convert into~}}\lintime\ \lba}$
			\item<4-> number of visits of each cell is bounded by a constant~$K$
			\item<6-> given~$K$, simulation by a \owdfa\qed
		\end{itemize}%
	\end{inlineprooof}
	\begin{overlayarea}{\textwidth}{.5\textheight}
		\widebox[1]{%
			\foreach \time [count=\page from 1] in {2,3,4,5-6}{%
				\only<\time>{\includegraphics[page=\page,width=\paperwidth]{Hennie-proof-tape}}%
			}%
		}
		\only<7->{%
			\begin{overlayarea}{\textwidth}{.45\textheight}
				\rightwidebox{%
					\textbf{Limitations:}
					\begin{itemize}
						\item<7-> no recursive function bounds~$K$
						\item<7-> \textsl{“Is a given \Tm \lintime?”} is undecidable
						\item<7-> non-recursive size-cost for the conversion of \lintime \Tm into \owdfa
					\end{itemize}
				}%
				\bigskip
				\uncover<8->{%
					\textbf{Syntactic restriction:}\cite[font=\normalsize]{Prusa'14}
					\begin{itemize}
						\item<8->\alert<8>{\emph{\alert<9->weight-\alert<9->reducing}} \Tm\s:
							each rewriting is decreasing
					\end{itemize}
					\smallskip

					\widebox[.25]{%
						\uncover<9->{\textbf{Rk:} given $K$, \lintime \Tm$\xrightarrow{\text{linear($K$)}}$\wreduc\Tm}
						\hfill
						\uncover<10->{\textbf{Thm:} \wreduc\Tm$\xrightarrow{\expexp}$\owdfa}
					}
				}
			\end{overlayarea}
		}
	\end{overlayarea}
\end{frame}
\section{Main result}
\begin{frame}{Main result}
	\foreach \time/\page in {1/14,2-/16}{%
		\only<\time>{\includegraphics[page=\page,width=\textwidth]{descr-compl-reg}}%
	}
	\begin{inlinetheorem}<2->%
		Each $n$-state \textbf{nondeterministic} \twfa
		can be converted%
		\medskip

		into a \alert<2>{$\poly(n)$}-size \textbf{deterministic} \wreduc\Tm{}%
	\end{inlinetheorem}
\end{frame}
\begin{frame}{From \twnfa to \wreduc\Tm: proof ideas}
	\rightwidebox{%
		\begin{enumerate}[label={\bfseries Ingredient~\arabic*:},leftmargin=4em]
			\item<1-> Shepherdson's construction ($\twfa\xrightarrow{\text{convert into}}\owfa$) \cite[font=\normalsize]{She'59}
			\item<7-> Powerset construction
		\end{enumerate}
	}
	\vfill
	\widebox[1]{%
		\foreach \time [count=\page] in {1,...,7}{%
			\only<\time>{\includegraphics[page=\page,width=\paperwidth]{construction}}%
		}%
	}%
\end{frame}
\begin{frame}{Conclusion}
	\begin{inlinetheorem}<1->%
		Each $n$-state \textbf{nondeterministic} \twfa
		can be converted%
		\medskip

		into a \alert<1>{$\poly(n)$}-size \textbf{deterministic} \wreduc\Tm{}%
	\end{inlinetheorem}
	\bigskip
	\begin{overlayarea}{\textwidth}{16ex}
		\widebox{%
			\centering 
			\only<2->{%
				\huge
				Can we obtain a deterministic%
				\nit[anchor=base,nit,text width=4em,text height=2ex,align=center,inner sep=0]{%
					\alert<12>{\only<12>{\bfseries\Huge}\wreduc\lba}%
				}\!\!?%
			}%
			\only<1>{\includegraphics[page=17,height=16ex]{descr-compl-reg}}%
		}
	\end{overlayarea}
	\vspace{-15ex}
	\begin{inlinetheorem}<3->
		\rightwidebox[.75]{%
			\begin{itemize}
				\item<3-> possible if we require equivalence only on \alert<3>{long inputs}
					\hfill
					$\length u\!\geq\!n^2$
					\smallskip

					\rightwidebox[.25]<4->{%
						\small\,\,
						Sakoda\&Sipser problem is already
						%\nit[anchor=base,nit,text width=11em,align=left,inner sep=0]{%
							\alert<4,11>{\only<11>{\bfseries\large}hard on short inputs}%
						%}%
						%}[short inputs][text width=width("\large\bfseries hard on short inputs"),align=center]%
						\cite{Kapoutsis'14}%
					}%
					\smallskip
				\item<5-> possible if the input alphabet is \alert<5>{unary}
				\item<6-> \alert<6,10-11>{\only<10-11>{\bfseries\Large} open otherwise}
					\begin{itemize}
						\item<7-> conversion of \twnfa into $\poly$-size \alert<7>{non-\wreduc} \lintime \lba

							\hfill
							\uncover<8->{with~$\nit{$K$}[K]\in\bigo{\alert<8>{n^{\log n}}}$}
							\tikz[overlay,remember picture,visible on={<8>}]
							\path[draw]
								(K) ++(270:4ex) ++(180:2em)
								node[anchor=east,shape=rectangle,rounded corners,draw=black]	(K def) {bound on cell visit}
								(K def)	[-stealth]-|	(K)
								;
					\end{itemize}
			\end{itemize}
		}
	\end{inlinetheorem}
	\vspace{0ex}
	\widebox[0]<9->{\includegraphics[page=14,height=12ex]{descr-compl-reg}}
	\vspace{-6ex}
	\rightwidebox<13->{%
		{\hfill\alert<13>{\Large \fbox{Thank you!}}}%
	}%
\end{frame}
\end{document}
